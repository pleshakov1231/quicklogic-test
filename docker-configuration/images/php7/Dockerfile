FROM composer:latest AS composer
FROM php:7.4-fpm

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN apt-get update && \
    apt-get install -y nano \
                       sudo \
                       wget

RUN  apt-get update && \
    apt-get install -y libpq-dev \
  libxml2-dev \
  libzip-dev \
  libpng-dev \
  libjpeg-dev \
  libfreetype6-dev \
  zip \
  uuid-dev\
  librabbitmq-dev \
  && docker-php-ext-configure zip \
  && docker-php-ext-configure gd \
  && pecl install amqp \
  && docker-php-ext-install \
    zip \
    gd \
    pdo_pgsql \
    soap \
    bcmath \
    opcache \
    pcntl \
    intl

RUN apt-get install -y autoconf g++ make git \
  && docker-php-source extract \
  && pecl install \
    redis-5.1.1 \
    apcu \
    uuid \
  && docker-php-ext-enable \
    redis \
    apcu \
    uuid \
    amqp \
  && docker-php-source delete \
  && mkdir /src && cd /src && git clone -b '2.9.6' https://github.com/xdebug/xdebug.git && cd xdebug && sh ./rebuild.sh && cd / && rm -fr /src \
  && rm -rf /tmp/*

COPY conf.d/* /usr/local/etc/php/conf.d/

# PHP cs fixer
RUN wget https://cs.symfony.com/download/php-cs-fixer-v2.phar -O php-cs-fixer && \
    sudo chmod a+x php-cs-fixer \
    && sudo mv php-cs-fixer /usr/local/bin/php-cs-fixer

ARG USER_ID
ARG GROUP_ID

RUN usermod -u ${USER_ID} www-data && groupmod -g ${GROUP_ID} www-data

WORKDIR /var/www

USER "${USER_ID}:${GROUP_ID}"

CMD ["php-fpm"]

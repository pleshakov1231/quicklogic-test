<?php

namespace App\Tests\Entity;

use App\Entity\Asteroid;
use PHPUnit\Framework\TestCase;

class AsteroidTest extends TestCase
{
    /**
     * @var string
     */
    private $reference = '3836101';

    /**
     * @var string
     */
    private $name = '(2018 VE4)';

    /**
     * @var float
     */
    private $speed = 188899.98;

    /**
     * @var bool
     */
    private $is_hazardous = false;

    /**
     * @var Asteroid
     */
    private $model;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->model = new Asteroid();
        $this->model->setName($this->name);
        $this->model->setReference($this->reference);
        $this->model->setSpeed($this->speed);
        $this->model->setIsHazardous($this->is_hazardous);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->model = null;
        $this->model = null;
        $this->model = null;
        $this->model = null;
        $this->model = null;
    }

    public function testRequiredFields()
    {
        $this->assertEquals($this->name, $this->model->getName());
        $this->assertEquals($this->reference, $this->model->getReference());
        $this->assertEquals($this->speed, $this->model->getSpeed());
        $this->assertEquals($this->is_hazardous, $this->model->getIsHazardous());
    }
}

<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Hazardous
{
    /**
     * @Assert\Choice(
     *     choices = { "true", "false" },
     *     message = "Choose a valid hazardous parameter (true|false)."
     * )
     */
    protected $hazardous;

    /**
     * @return mixed
     */
    public function getHazardous()
    {
        return $this->hazardous;
    }

    /**
     * @param $hazardous
     *
     * @return mixed
     */
    public function setHazardous($hazardous)
    {
        return $this->hazardous = $hazardous;
    }
}

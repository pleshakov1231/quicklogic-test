<?php

namespace App\Controller;

use App\Entity\Hazardous;
use App\Repository\AsteroidRepository;
use App\Service\ValidateQueryErrorsService\ValidateQueryErrorsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class GetFastestController extends AbstractController
{
    /**
     * @Route("/neo/fastest", name="fastest", methods={"GET"})
     *
     * @param Request                    $request
     * @param AsteroidRepository         $asteroidRepository
     * @param SerializerInterface        $serializer
     * @param ValidateQueryErrorsService $queryErrorsService
     *
     * @return Response
     */
    public function getFastest(Request $request, AsteroidRepository $asteroidRepository, SerializerInterface $serializer, ValidateQueryErrorsService $queryErrorsService)
    {
        $hazardous = new Hazardous();
        $hazardous->setHazardous($request->query->get('hazardous'));

        $errorMessages = $queryErrorsService->getQueryErrors($hazardous);
        if (!empty($errorMessages)) {
            return new Response(json_encode($errorMessages), Response::HTTP_BAD_REQUEST);
        }

        $hazardous = $hazardous->getHazardous() === 'true' ? true : false;
        $asteroid = $asteroidRepository->findFastest($hazardous);
        $jsonResponse = $serializer->serialize($asteroid, 'json');

        return new Response($jsonResponse, Response::HTTP_OK);
    }
}

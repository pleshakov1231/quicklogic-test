<?php

namespace App\Controller;

use App\Entity\Hazardous;
use App\Service\BestMonthService\BestMonthService;
use App\Service\ValidateQueryErrorsService\ValidateQueryErrorsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetMonthWithAsteroidsController extends AbstractController
{
    /**
     * @Route("/neo/best-month", name="best-month", methods={"GET"})
     *
     * @param Request                    $request
     * @param BestMonthService           $bestMonthService
     * @param ValidateQueryErrorsService $queryErrorsService
     *
     * @return Response
     */
    public function getMonthWithMostAsteroids(Request $request, BestMonthService $bestMonthService, ValidateQueryErrorsService $queryErrorsService)
    {
        $hazardous = new Hazardous();
        $hazardous->setHazardous($request->query->get('hazardous'));

        $errorMessages = $queryErrorsService->getQueryErrors($hazardous);
        if (!empty($errorMessages)) {
            return new Response(json_encode($errorMessages), Response::HTTP_BAD_REQUEST);
        }

        $bestMonth = $bestMonthService->getBestMonth($hazardous);

        return new Response(json_encode($bestMonth), Response::HTTP_OK);
    }
}

<?php

namespace App\Controller;

use App\Repository\AsteroidRepository;
use App\Service\ResponsePaginationStructureService\ResponsePaginationStructureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GetHazardousController.
 */
class GetHazardousController extends AbstractController
{
    use SerializerTrait;

    /**
     * @Route("/neo/hazardous", name="hazardous", methods={"GET"})
     *
     * @param Request                            $request
     * @param AsteroidRepository                 $asteroidRepository
     * @param ResponsePaginationStructureService $paginationStructureService
     *
     * @return Response
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function getHazardous(Request $request, AsteroidRepository $asteroidRepository, ResponsePaginationStructureService $paginationStructureService): Response
    {
        $asteroidsQuery = $asteroidRepository->findByHazardous();
        $paginatedCollection = $paginationStructureService->getPaginationCollection($asteroidsQuery, $request, 'hazardous');

        $this->initSerializer();
        $jsonResponse = $this->serializer->serialize($paginatedCollection, 'json');

        return new Response($jsonResponse, Response::HTTP_OK);
    }
}

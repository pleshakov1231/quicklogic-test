<?php

namespace App\Pagination;

/**
 * Class PaginatedCollection.
 */
class PaginatedCollection
{
    /**
     * @var array
     */
    private array $items;

    /**
     * @var int
     */
    private int $total;

    /**
     * @var int
     */
    private int  $count;

    /**
     * @var array
     */
    private array $_links = [];

    /**
     * PaginatedCollection constructor.
     *
     * @param array $items
     * @param int   $totalItems
     * @param int   $count
     */
    public function __construct(array $items, int $totalItems, int $count)
    {
        $this->items = $items;
        $this->total = $totalItems;
        $this->count = $count;
    }

    /**
     * @param string $ref
     * @param string $url
     */
    public function addLink(string $ref, string $url)
    {
        $this->_links[$ref] = $url;
    }
}

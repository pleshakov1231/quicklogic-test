<?php

namespace App\Service\ValidateQueryErrorsService;

use App\Entity\Hazardous;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidateQueryErrorsService
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Hazardous $hazardous
     *
     * @return array
     */
    public function getQueryErrors(Hazardous $hazardous): array
    {
        $this->validator->validate($hazardous);

        $errors = $this->validator->validate($hazardous);
        $messages = [];
        if (count($errors) > 0) {
            foreach ($errors as $violation) {
                $messages[$violation->getPropertyPath()][] = $violation->getMessage();
            }
        }

        return $messages;
    }
}

<?php

namespace App\Service\BestMonthService;

use App\Entity\Hazardous;
use App\Repository\AsteroidRepository;

class BestMonthService
{
    const EMPTY_RESPONSE = 'No month with these search parameters';

    private AsteroidRepository $asteroidRepository;

    public function __construct(AsteroidRepository $asteroidRepository)
    {
        $this->asteroidRepository = $asteroidRepository;
    }

    /**
     * @param Hazardous $hazardous
     *
     * @return array
     */
    public function getBestMonth(Hazardous $hazardous): array
    {
        $mostMonth = $this->calculateAsteroidsInMonth($hazardous);
        $responseArray = [];

        if (!empty($mostMonth)) {
            $dateObj = \DateTime::createFromFormat('!m',  intval($mostMonth));
            $monthName = $dateObj->format('F');
            $responseArray[] = $monthName;
        } else {
            $responseArray[] = self::EMPTY_RESPONSE;
        }

        return $responseArray;
    }

    /**
     * @param Hazardous $hazardous
     *
     * @return string
     */
    private function calculateAsteroidsInMonth(Hazardous $hazardous): string
    {
        $hazardous = $hazardous->getHazardous() === 'true' ? true : false;
        $asteroidCountByMonth = $this->asteroidRepository->findMountWithMostAsteroids($hazardous);

        $monthCnt = 0;
        $mostMonth = '';
        if (!empty($asteroidCountByMonth)) {
            foreach ($asteroidCountByMonth as $month) {
                if ($month['cnt'] > $monthCnt) {
                    $monthCnt = $month['cnt'];
                    $mostMonth = $month['dateAsMonth'];
                }
            }
        }

        return $mostMonth;
    }
}

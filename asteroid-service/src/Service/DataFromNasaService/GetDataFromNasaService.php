<?php

namespace App\Service\DataFromNasaService;

use App\Entity\Asteroid;
use App\Repository\AsteroidRepository;
use App\Service\DataFromNasaService\Exception\ErrorResponseAsteroidDataException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetDataFromNasaService
{
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * @var AsteroidRepository
     */
    private AsteroidRepository $asteroidRepository;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * GetDataFromNasaService constructor.
     *
     * @param HttpClientInterface $httpClient
     */
    public function __construct(HttpClientInterface $httpClient, AsteroidRepository $asteroidRepository, EntityManagerInterface $em)
    {
        $this->httpClient = $httpClient;
        $this->asteroidRepository = $asteroidRepository;
        $this->em = $em;
    }

    public function process(): void
    {
        $asteroidDataArray = $this->getAsteroidData();
        $this->storeAsteroidData($asteroidDataArray);
    }

    /**
     * @return array
     *
     * @throws ErrorResponseAsteroidDataException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function getAsteroidData(): array
    {
        try {
            $response = $this->httpClient->request('GET', 'https://api.nasa.gov/neo/rest/v1/feed', $this->getRequestOptions());

            return json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (HttpExceptionInterface $exception) {
            throw new ErrorResponseAsteroidDataException($exception->getMessage());
        } catch (\JsonException $exception) {
            throw new ErrorResponseAsteroidDataException($exception->getMessage());
        }
    }

    /**
     * @param array $asteroidDataArray
     *
     * @throws \Exception
     */
    private function storeAsteroidData(array $asteroidDataArray): void
    {
        foreach ($asteroidDataArray['near_earth_objects'] as $date => $dataByDay) {
            foreach ($dataByDay as $asteroidData) {
                $asteroid = $this->asteroidRepository->findOneBy(['reference' => $asteroidData['neo_reference_id']]);
                if (is_null($asteroid)) {
                    $asteroid = new Asteroid();
                    $asteroid->setDate(new \DateTime($asteroidData['close_approach_data'][0]['close_approach_date']));
                    $asteroid->setName($asteroidData['name']);
                    $asteroid->setReference($asteroidData['neo_reference_id']);
                    $asteroid->setSpeed(floatval($asteroidData['close_approach_data'][0]['relative_velocity']['kilometers_per_hour']));
                    $this->em->persist($asteroid);
                }
            }
        }
        $this->em->flush();
    }

    /**
     * @return array[]
     */
    private function getRequestOptions(): array
    {
        $endDate = new \DateTime();
        $endDateFormat = $endDate->format('Y-m-d');
        $startDate = $endDate->modify('-3 day');

        return [
            'query' => [
                'start_date' => $startDate->format('Y-m-d'),
                'end_date' => $endDateFormat,
                'api_key' => 'N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD',
            ],
        ];
    }
}

<?php

namespace App\Service\DataFromNasaService\Exception;

use Exception;

class ErrorResponseAsteroidDataException extends Exception
{
}

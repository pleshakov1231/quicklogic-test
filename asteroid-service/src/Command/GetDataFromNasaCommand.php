<?php

namespace App\Command;

use App\Service\DataFromNasaService\GetDataFromNasaService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetDataFromNasaCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'store-asteroids';

    /**
     * @var GetDataFromNasaService
     */
    private GetDataFromNasaService $asteroidService;

    public function __construct(GetDataFromNasaService $asteroidService)
    {
        $this->asteroidService = $asteroidService;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->asteroidService->process();

        return Command::SUCCESS;
    }
}

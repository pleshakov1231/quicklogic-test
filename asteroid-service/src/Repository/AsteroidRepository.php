<?php

namespace App\Repository;

use App\Entity\Asteroid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Asteroid|null find($id, $lockMode = null, $lockVersion = null)
 * @method Asteroid|null findOneBy(array $criteria, array $orderBy = null)
 * @method Asteroid[]    findAll()
 * @method Asteroid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AsteroidRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Asteroid::class);
    }

    public function findByHazardous($value = true)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.is_hazardous = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
        ;
    }

    public function findFastest(bool $param)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.is_hazardous = :param')
            ->addOrderBy('s.speed', 'DESC')
            ->setMaxResults(1)
            ->setParameter(':param', $param)
            ->getQuery()
            ->getResult();
    }

    public function findMountWithMostAsteroids(bool $param)
    {
        return $this->createQueryBuilder('s')
            ->select("date_format(s.date, 'mm') as dateAsMonth", 'COUNT(s.date) as cnt')
            ->andWhere('s.is_hazardous = :param')
            ->setParameter(':param', $param)
            ->groupBy('dateAsMonth')
            ->getQuery()
            ->getResult();
    }
}

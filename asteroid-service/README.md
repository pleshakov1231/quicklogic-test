

## Useful commands:

- **Enter to container:**
```
docker exec -it asteroid-service bash
```

- **Go to directory:**
```
cd asteroid-service.loc
```

- **Install composer:**
```
composer install
```

- **Run migrations:**
```
bin/console doctrine:migrations:migrate
```

- **Add data to DB:**
```
bin/console store-asteroids
```


```
Username: user
Password: password 
System: PostgreSQL
Server: specify DB container name (pgsql_asteroid_service)
```